#pragma once
#include <SDL2/SDL_events.h>

namespace SDL
{
	namespace Events
	{
		using Event = SDL_Event;		
	}
}
