#pragma once
#include <Queue>
#include <SDL2\SDL.h>
#include <functional>
#include "event.h"

namespace SDL
{
	class Window 
	{
	public:
		using event_callback = std::function<void(SDL::Events::Event)>;
		//using association = std::pair<event_callback, Uint32>;

		enum class Hook {Window, Keyboard, Mouse};
		Window();
		~Window();

		void hook(event_callback cbk);
		void unhook(event_callback cbk);
		void close();
		SDL_GLContext openGL;
		//---
		
		inline void
		fullscreen()
		{
			SDL_SetWindowFullscreen(mWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
		}

		inline void
		resizable(bool is)
		{
			SDL_SetWindowResizable(mWindow, static_cast<SDL_bool>(is));
		}

		inline void
		hide()
		{
			SDL_HideWindow(mWindow);
		}

		inline void
		show()
		{
			SDL_ShowWindow(mWindow);
		}

		inline void
		maximise()
		{
			SDL_MaximizeWindow(mWindow);
		}

		inline void
		minimise()
		{
			SDL_MinimizeWindow(mWindow);
		}

		inline void
		restore()
		{
			SDL_RestoreWindow(mWindow);
		}

		inline void
		raise()
		{
			SDL_RaiseWindow(mWindow);
		}

		inline const std::string
		title()
		{
			return SDL_GetWindowTitle(mWindow);
		}

		inline void
		title(const std::string & title)
		{
			return SDL_SetWindowTitle(mWindow, title.c_str());
		}

		inline void
		borderless(bool hasBorder)
		{
			SDL_SetWindowBordered(mWindow, static_cast<SDL_bool>(hasBorder));
		}

		void simpleMessageBox();

		inline void
		makeGL_Context()
		{
			openGL = SDL_GL_CreateContext(mWindow);
			auto var = SDL_GetError();
		}

		inline void
		glSwap()
		{
			SDL_GL_SwapWindow(mWindow);
		}

	private:
		std::vector<event_callback> mWindowCallbacks;
		SDL_Window * mWindow;
		static Uint8 smWindowCount;
		bool mCallbackSeqInvalid;

		static int SDLCALL window_events(void* data, SDL::Events::Event* ev);
	};
}