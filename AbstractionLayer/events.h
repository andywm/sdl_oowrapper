#pragma once
#include <memory>
#include <vector>
#include <SDL2\SDL.h>
#include "event.h"

namespace SDL
{
	namespace Events
	{
		/*******************************
				Design Notice
				*************
			Namespaces have been choosen
			over enum classes as the use
			case syntax is nicer.

			> (int)EnumClass::Name
					vs
			> Namespace::Name

		********************************/

		//Base SDL Events
		//Lone
		enum
		{
			Quit					= SDL_QUIT,
			Custom					= SDL_USEREVENT,
			Clipboard_Update		= SDL_CLIPBOARDUPDATE
		};

		namespace Boundary
		//enum class Boundary
		{
			enum
			{
				First					= SDL_FIRSTEVENT,
				Last					= SDL_LASTEVENT
			};
		}

		//mobiles
		namespace App
		//enum class App
		{
			enum
			{
				Terminating				= SDL_APP_TERMINATING,
				Low_Memory				= SDL_APP_LOWMEMORY,
				Will_Enter_Background	= SDL_APP_WILLENTERBACKGROUND,
				Did_Enter_Background	= SDL_APP_DIDENTERBACKGROUND,
				Will_Enter_Foreground	= SDL_APP_WILLENTERFOREGROUND,
				Did_Enter_Foreground	= SDL_APP_DIDENTERFOREGROUND
			};
		}

		namespace Window
		//enum class Window
		{	
			enum
			{
				SystemWM				= SDL_SYSWMEVENT,
				Any						= SDL_WINDOWEVENT,

				None					= SDL_WINDOWEVENT_NONE,
				Shown					= SDL_WINDOWEVENT_SHOWN,
				Hidden					= SDL_WINDOWEVENT_HIDDEN,
				Exposed					= SDL_WINDOWEVENT_EXPOSED,
				Moved					= SDL_WINDOWEVENT_MOVED,
				Resized					= SDL_WINDOWEVENT_RESIZED,
				Size_Changed			= SDL_WINDOWEVENT_SIZE_CHANGED,
				Minimised				= SDL_WINDOWEVENT_MINIMIZED,
				Maximised				= SDL_WINDOWEVENT_MAXIMIZED,
				Restored				= SDL_WINDOWEVENT_RESTORED,
				Enter					= SDL_WINDOWEVENT_ENTER,
				Leave					= SDL_WINDOWEVENT_LEAVE,
				Focus_Gained			= SDL_WINDOWEVENT_FOCUS_GAINED,
				Focus_Lost				= SDL_WINDOWEVENT_FOCUS_LOST,
				Close					= SDL_WINDOWEVENT_CLOSE,
				Take_Focus				= SDL_WINDOWEVENT_TAKE_FOCUS,
				Hit_test 				= SDL_WINDOWEVENT_HIT_TEST
			};
		}

		namespace Keyboard
		//enum class Keyboard
		{
			enum
			{
				Key_Down				= SDL_KEYDOWN,
				Key_Up					= SDL_KEYUP,
				Key_Map_Changed			= SDL_KEYMAPCHANGED,
				Text_Editing			= SDL_TEXTEDITING,
				Text_Input				= SDL_TEXTINPUT
			};
		}

		namespace Mouse
		//enum class Mouse
		{
			enum
			{
				Motion					= SDL_MOUSEMOTION,
				Button_Down				= SDL_MOUSEBUTTONDOWN,
				Button_Up				= SDL_MOUSEBUTTONUP,
				Wheel					= SDL_MOUSEWHEEL
			};
		}

		namespace
		//enum class Joystick
		{
			enum
			{
				Axis_Motion				= SDL_JOYAXISMOTION,
				Ball_Motion				= SDL_JOYBALLMOTION,
				Hat_Motion				= SDL_JOYHATMOTION,
				Button_Down				= SDL_JOYBUTTONDOWN,
				Button_Up				= SDL_JOYBUTTONUP,    
				Device_Added			= SDL_JOYDEVICEADDED,
				Device_Removed			= SDL_JOYDEVICEREMOVED
			};
		}

		namespace GameController
		//enum class GameController
		{
			enum
			{
				Axis_Motion				= SDL_CONTROLLERAXISMOTION,
				Button_Down				= SDL_CONTROLLERBUTTONDOWN,
				Button_Up				= SDL_CONTROLLERBUTTONUP,
				Device_Added			= SDL_CONTROLLERDEVICEADDED,
				Device_Removed			= SDL_CONTROLLERDEVICEREMOVED,
				Device_Remapped			= SDL_CONTROLLERDEVICEREMAPPED
			};
		}

		namespace Audio
		//enum class Audio
		{ 
			enum
			{
				Device_Added			= SDL_AUDIODEVICEADDED,
				Device_Removed			= SDL_AUDIODEVICEREMOVED
			};
		}

		namespace Touch
		//enum class Touch
		{
			enum
			{
				Finger_Down				= SDL_FINGERDOWN,
				Finger_Up				= SDL_FINGERUP,
				Finger_Motion			= SDL_FINGERMOTION
			};
		}

		namespace Gesture
		//enum class Gesture
		{
			enum
			{
				Dollar					= SDL_DOLLARGESTURE,
				Dollar_Record			= SDL_DOLLARRECORD,
				Multi					= SDL_MULTIGESTURE
			};
		}

		namespace DragDrop
		//enum class DragDrop
		{
			enum
			{
				File					= SDL_DROPFILE,
				Text					= SDL_DROPTEXT,
				Begin					= SDL_DROPBEGIN,
				Complete				= SDL_DROPCOMPLETE,
			};
		}

		namespace Render
		//enum class Render
		{ 
			enum
			{
				Targets_Reset			= SDL_RENDER_TARGETS_RESET,
				Device_Reset			= SDL_RENDER_DEVICE_RESET
			};
		}
	}
}
