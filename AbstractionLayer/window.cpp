#include <SDL2\SDL.h>
#include "window.h"
#include "events.h"
namespace SDL
{
	Uint8 Window::smWindowCount = 0;

	Window::Window()
		:
		mWindow(nullptr),
		openGL(nullptr)
	{
		if (!SDL_WasInit(SDL_INIT_VIDEO))
		{
			//ensure SDL is initialised.
			if (SDL_Init(SDL_INIT_VIDEO) != 0)
			{
				//logger
				//**fatal**
				return;
			}
		}

		//attempt to create the window.
		mWindow = SDL_CreateWindow(
			"",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			500,
			500,
			SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

		if (mWindow == nullptr)
		{
			//logger
			//**fatal**
			return;
		}
		SDL_AddEventWatch(window_events, this);
	
		smWindowCount++;
	}

	Window::~Window()
	{
		SDL_DestroyWindow(mWindow);
		SDL_DelEventWatch(window_events, this);
		if(openGL !=nullptr)
		SDL_GL_DeleteContext(openGL);
		smWindowCount--;

		if (smWindowCount == 0)
		{
			SDL_VideoQuit();
		}
	}

	int SDLCALL
	Window::window_events(void* data, SDL::Events::Event* ev)
	{
		if (ev->type == SDL::Events::Window::Any)
		{
			auto win = static_cast<Window *>(data);
			if (ev->window.windowID == SDL_GetWindowID(win->mWindow))
			{
				std::vector<event_callback> callbacks = win->mWindowCallbacks;
				for (const auto cbk : callbacks)
				{
					cbk(*ev);
				}
			}
		}
		return 0;
	}

	template<typename T> bool 
	find_func(const T & object, 
		const std::vector<T> & list,
		int * location=nullptr)
	{
		int offset = 0;

		for (auto single : list)
		{
			if (single.target<T>() ==
				object.target<T>())
			{
				if (location != nullptr) *location = offset;
				return true;
			}
			offset++;
		}
		return false;
	}

	void 
	Window::close()
	{	
		//SDL_DelEventWatch(window_events, this);
		SDL_DestroyWindow(mWindow);
		auto v = SDL_GetError();
		mWindowCallbacks.clear();		
	}

	void
	Window::hook(event_callback cbk)
	{
		if (!find_func(cbk, mWindowCallbacks))
		{
			mWindowCallbacks.push_back(cbk);
		}
	}

//------------------------------------------------------------------------------

	void
	Window::unhook(event_callback cbk)
	{
		int offset = 0;
		if (find_func(cbk, mWindowCallbacks, &offset))
		{
			mWindowCallbacks.erase(mWindowCallbacks.begin() + offset);
		}
	}

	//---features

	void
	Window::simpleMessageBox()
	{
		SDL_ShowSimpleMessageBox(
			SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT,
			"test",
			"test",
			mWindow);
	}	
}

/*
	Features...
*/


//Maximise
//Minimise
//Show
//Hide
//Size
//Title
//Opacity
//Raise
//Restore
//Icon
//Resizable
//Border
//Fullscreen
//

//MessageBox